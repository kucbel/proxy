# Proxy

This extension can generate lazy loading proxies for your services.

**Usage**

* Install using composer `composer require kucbel/proxy`
* Register the extension `Kucbel\Proxy\DI\ProxyExtension` in your config
* Add tag `proxy` to any service