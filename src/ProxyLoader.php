<?php declare(strict_types=1);

namespace Kucbel\Proxy;

use Closure;
use Nette\Utils\ObjectHelpers;

class ProxyLoader
{
	protected Closure
		$factory;

	protected object
		$service;


	function __construct( Closure $factory )
	{
		$this->factory = $factory;
	}


	protected function create() : object
	{
		return $this->service ??= ( $this->factory )();
	}


	function __call( string $name, array $arguments ) : mixed
	{
		$service = $this->create();

		return $service->$name( ...$arguments );
	}


	static function __callStatic( string $name, array $arguments )
	{
		ObjectHelpers::strictStaticCall( static::class, $name );
	}


	function __get( string $name ) : mixed
	{
		$service = $this->create();

		return $service->$name;
	}


	function __set( string $name, mixed $value ) : void
	{
		$service = $this->create();
		$service->$name = $value;
	}


	function __isset( string $name ) : bool
	{
		$service = $this->create();

		return isset( $service->$name );
	}


	function __unset( string $name ) : void
	{
		$service = $this->create();

		unset( $service->$name );
	}


	static function unpack( object $loader ) : void
	{
		if( $loader instanceof self ) {
			$loader->create();
		}
	}
}
