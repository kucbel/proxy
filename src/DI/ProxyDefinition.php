<?php declare(strict_types=1);

namespace Kucbel\Proxy\DI;

use Kucbel\Proxy\Proxy;
use Kucbel\Proxy\ProxyFactory;
use Nette\DI\Definitions\Definition;
use Nette\DI\PhpGenerator;
use Nette\DI\Resolver;
use Nette\InvalidStateException;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpFile;
use Nette\Utils\FileSystem;

final class ProxyDefinition extends Definition
{
	private Definition
		$service;

	private ProxyFactory
		$factory;

	private string
		$folder;

	private string
		$source;


	function __construct( Definition $service, string $folder )
	{
		$this->service = $service;
		$this->folder = $folder;

		$this->setAutowired( false );
		$this->setType( Proxy::class );
	}


	function resolveType( Resolver $resolver ) : void
	{

	}


	function complete( Resolver $resolver ) : void
	{
		$type = $this->service->getType();
		$name = $this->service->getName();

		if( !$type ) {
			throw new InvalidStateException("Proxy service doesn't have a type.");
		} elseif( !$name ) {
			throw new InvalidStateException("Proxy service doesn't have a name.");
		}

		$path = $this->folder;
		$hash = md5( $type );

		$this->factory = new ProxyFactory( $type );
		$this->source = "{$path}/{$hash}.php";
	}


	function generateMethod( Method $method, PhpGenerator $generator ) : void
	{
		$proxy = $this->generate();

		$method->addBody("require_once '{$this->source}';");
		$method->addBody('');
		$method->addBody("return new {$proxy}( \$this, '{$this->service->getName()}');");
	}


	private function generate() : string
	{
		$proxy = $this->factory->create( $source = new PhpFile );

		$render = (string) $source;

		if( !is_file( $this->source )) {
			FileSystem::write( $this->source, $render );
		} elseif( file_get_contents( $this->source ) !== $render ) {
			FileSystem::delete( $this->source );
			FileSystem::write( $this->source, $render );
		}

		foreach( $source->getNamespaces() as $space ) {
			return "{$space->getName()}\\{$proxy->getName()}";
		}

		return $proxy->getName();
	}
}
