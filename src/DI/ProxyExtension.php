<?php // NO!

namespace Kucbel\Proxy\DI;

use Nette\DI\CompilerExtension;
use Nette\InvalidStateException;
use Nette\PhpGenerator\ClassType;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Nette\Utils\Strings;

class ProxyExtension extends CompilerExtension
{
	private array
		$proxies = [];

	private string
		$folder;


	function loadConfiguration() : void
	{
		if( !$this->config->temp ) {
			throw new InvalidStateException("Temp dir must be specified.");
		} elseif( !is_dir( $this->config->temp )) {
			throw new InvalidStateException("Temp dir must exist.");
		}

		$this->folder = "{$this->config->temp}/proxy";
	}


	function beforeCompile() : void
	{
		$builder = $this->getContainerBuilder();

		foreach( $builder->getDefinitions() as $name => $service ) {
			if( !$service->getTag( $this->config->tag )) {
				continue;
			}

			$proxy = "{$name}.{$this->config->name}";

			$builder->addDefinition( $proxy, new ProxyDefinition( $service, $this->folder ));

			$this->proxies[ $name ] = $proxy;
		}
	}


	function afterCompile( ClassType $class ) : void
	{
		if( !$this->proxies ) {
			return;
		}

		$replace = function( array $match ) : string {
			$proxy = $this->proxies[ $match[1] ] ?? $match[1];

			return "getService('{$proxy}')";
		};

		foreach( $class->getMethods() as $method ) {
			if( $content = $method->getBody() ) {
				$method->setBody( Strings::replace( $content, '~getService\(\'([^\']+)\'\)~', $replace ));
			}
		}
	}


	function getConfigSchema() : Schema
	{
		$builder = $this->getContainerBuilder();

		return Expect::structure([
			'tag'	=> Expect::string('proxy')->min( 1 ),
			'name'	=> Expect::string('proxy')->min( 1 )->pattern('[a-zA-Z0-9]+([.][a-zA-Z0-9]+)*'),
			'temp'	=> Expect::string( $builder->parameters['tempDir'] ?? null )->assert('is_dir', 'Temp must be a directory.'),
		]);
	}
}
