<?php declare(strict_types=1);

namespace Kucbel\Proxy;

use Closure;

class ProxyCloner extends ProxyLoader
{
	protected ProxyLoader
		$loader;


	function __construct( ProxyLoader $loader, Closure $factory )
	{
		$this->loader = $loader instanceof self ? $loader->loader : $loader;

		parent::__construct( $factory );
	}


	protected function create() : object
	{
		return $this->service ??= ( $this->factory )( $this->loader->create() );
	}


	static function from( object $parent, Closure $factory ) : object
	{
		if( $parent instanceof ProxyLoader ) {
			return new self( $parent, $factory );
		} else {
			return clone $parent;
		}
	}
}