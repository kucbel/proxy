<?php declare(strict_types=1);

namespace Kucbel\Proxy;

use Nette\DI\Container;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Factory;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\PhpFile;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class ProxyFactory
{
	protected ReflectionClass
		$object;

	protected Factory
		$reader;

	protected string
		$loader = ProxyLoader::class;

	protected string
		$cloner = ProxyCloner::class;

	protected string
		$suffix = 'Proxy';

	protected string
		$parent = 'parent_';

	protected array
		$methods = [
			'__construct', '__destruct', '__clone', '__call', '__get', '__set', '__isset',
			'__unset', '__sleep', '__wakeup', '__invoke', '__toString', '__debugInfo',
		];


	function __construct( string $class )
	{
		$this->object = new ReflectionClass( $class );
		$this->reader = new Factory;

		$this->parent .= substr( md5( $class ), 0, 4 );
	}


	function create( PhpFile $source ) : ClassType
	{
		if( $this->object->inNamespace() ){
			$wrapper = $source->addNamespace( $this->object->getNamespaceName() );
			$factory = $wrapper->addClass( $this->object->getShortName() . $this->suffix );
		} else {
			$factory = $source->addClass( $this->object->getName() . $this->suffix );
		}

		$factory->addTrait( ProxyObject::class );
		$factory->addImplement( Proxy::class );

		if( $this->object->isInterface() ) {
			$factory->addImplement( $this->object->getName() );
		} else {
			$factory->setExtends( $this->object->getName() );
		}

		$factory->addProperty( $this->parent )
			->setVisibility('private')
			->setComment("@var {$this->object->getShortName()}");

		$method = $factory->addMethod('__construct')
			->addBody("\$this->{$this->parent} = new \\{$this->loader}( function() use( \$container, \$service ) {")
			->addBody("	return \$this->{$this->parent} = \$container->getService( \$service );")
			->addBody("});");

		$method->addParameter('container')
			->setType( Container::class );

		$method->addParameter('service')
			->setType( 'string');

		$factory->addMethod('__clone')
			->addBody("\$this->{$this->parent} = \\{$this->cloner}::from( \$this->{$this->parent}, function( \$parent ) {")
			->addBody("	return \$this->{$this->parent} = clone \$parent;")
			->addBody("});");

		$remove = [];

		foreach( $this->object->getProperties() as $property ) {
			if( $this->isPublicProperty( $property ) or $this->isProtectedProperty( $property )) {
				$remove[] = "\$this->{$property->getName()}";
			}
		}

		if( $remove ) {
			$remove = implode(', ', $remove );

			$method->addBody('');
			$method->addBody("unset( {$remove} );");
		}

		$replace = $factory->getMethods();

		foreach( $this->object->getMethods() as $method ) {
			if( $this->isPublicMethod( $method ) and !$this->isIgnoredMethod( $method )) {
				$replace[ $method->getName() ] = $this->override( $method );
			}
		}

		$factory->setMethods( $replace );

		return $factory;
	}


	protected function override( ReflectionMethod $method ) : Method
	{
		$startup = false;

		$factory = $this->reader->fromMethodReflection( $method );
		$factory->setComment( null );
		$factory->setBody('');

		foreach( $factory->getParameters() as $parameter ) {
			if( $parameter->isReference() ) {
				$startup = true;
			}

			if( $parameter->getType() === 'self') {
				$parameter->setType( $this->object->getName() );
			} elseif( $parameter->getType() === 'parent') {
				if( $parent = $this->object->getParentClass() ) {
					$parameter->setType( $parent->getName() );
				}
			}
		}

		if( $factory->getReturnType() === 'self') {
			$factory->setReturnType( $this->object->getName() );
		} elseif( $factory->getReturnType() === 'parent') {
			if( $parent = $this->object->getParentClass() ) {
				$factory->setReturnType( $parent->getName() );
			}
		}

		if( $startup ) {
			$factory->addBody("\\{$this->loader}::unpack( \$this->{$this->parent} );");
			$factory->addBody('');
		}

		$return = $factory->getReturnType();
		$prefix =
		$suffix = '';

		if( $return === 'static') {
			$suffix = "\n\nreturn \$this;";
		} elseif( $return !== 'void') {
			$prefix = 'return ';
		}

		$invoke = [];

		foreach( $method->getParameters() as $parameter ) {
			$dollar = $parameter->isVariadic() ? '...$' : '$';

			$invoke[] = $dollar . $parameter->getName();
		}

		$invoke = implode(', ', $invoke );

		$factory->addBody("{$prefix}\$this->{$this->parent}->{$method->getName()}({$invoke});{$suffix}");

		return $factory;
	}


	protected function isProtectedProperty( ReflectionProperty $property ) : bool
	{
		return $property->isProtected() and !$property->isStatic();
	}


	protected function isPublicProperty( ReflectionProperty $property ) : bool
	{
		return $property->isPublic() and !$property->isStatic();
	}


	protected function isPublicMethod( ReflectionMethod $method ) : bool
	{
		return $method->isPublic() and !$method->isStatic();
	}


	protected function isIgnoredMethod( ReflectionMethod $method ) : bool
	{
		return in_array( $method->getName(), $this->methods, true );
	}
}
