<?php declare(strict_types=1);

namespace Kucbel\Proxy;

use Nette\NotImplementedException;

trait ProxyObject
{
	function __sleep()
	{
		throw new NotImplementedException("Proxy serialization isn't implemented.");
	}
}
